﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectRobot
{
    internal class Killer : Robot
    {
        public int Health { get; set; }

        public Killer() { }

        public Killer(string name, int weight, byte[] coordinates, int health) : base(name, weight, coordinates)
        {
            this.Health = health;
            base.printValues();
        }

        public void lazer()
        {
            Console.WriteLine("Lazer shooting.");
            base.surname = "Doe";
        }
    }
}
