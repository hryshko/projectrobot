﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectRobot
{
    internal class Robot { 

        private static int count;
    
        private string name;
        private int weight;
        private byte [] coordinates;

        protected string surname;

        public int Weight{
            get {
                Console.WriteLine("Rezult: ");
                return this.weight;
            }
            set {
                if (value < 1)
                    this.weight = 1;
                else 
                this.weight = value;
            }

        }

        public int Width { get; set; }

        public Robot(string name, int weight, byte[] coordinates)
        {
            System.Console.WriteLine("Object has been created.");
            this.setValues(name, weight, coordinates);
            count++;
        }
        public Robot() {
            count++;
        }

        public Robot(string name) {
            System.Console.WriteLine("Object has been created.");
            this.name = name;
            count++;
        }

        public void setValues(string name, int weight, byte[] coordinates)
        {
            this.name = name;
            this.weight = weight;
            this.coordinates =  coordinates;          
           
        }

        public  void printValues()
        {
            System.Console.WriteLine(name, weight);
            foreach (byte coordinates in coordinates)
                Console.WriteLine(coordinates);
            
        }
       
    }
}
